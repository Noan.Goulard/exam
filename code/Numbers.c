#include <stdbool.h>
#include <stdio.h>

#define TAILLE 50

/// Returns the prime numbers in `elements`.
void primes(int numbers[]) {
  int i;
  for (i=0; i<TAILLE; i++) {
    bool isPrime = true;
    int m;
    for (m=2; m < numbers[i] ;m++) {
      if (numbers[i] % m == 0) {
        isPrime = false;
        break;
      }
    }

    if (isPrime) {
      printf("%i est un nombre premier\n", numbers[i]);  }  
  }
}

int main() {
  int array[TAILLE];
  int i;
  for (i=0; i<TAILLE; i++)
    array[i] = i+3;

  primes(array);
}