La commande pour obtenir le pourcentage d'utilisation de "X" est :

grep "X" data.csv | cut -f 3 -d ,
